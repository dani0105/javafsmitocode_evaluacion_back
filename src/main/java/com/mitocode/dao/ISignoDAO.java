package com.mitocode.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mitocode.model.Consulta;
import com.mitocode.model.Signo;

public interface ISignoDAO extends JpaRepository<Signo, Integer>{

	@Query(value="from Signo s where s.id_paciente = :idPaciente", nativeQuery = true)
	List<Consulta> buscarPorIdPaciente(@Param("idPaciente") Integer idPaciente);
}
