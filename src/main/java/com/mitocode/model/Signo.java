package com.mitocode.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "Signo")
public class Signo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idSigno;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_paciente", nullable = false)
	private Paciente paciente;

	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDateTime fecha;
	
	@ApiModelProperty(notes = "Temperatura debe tener minimo 1 caracter y máximo 3")
	@Size(min = 1, max = 3, message = "Temperatura debe tener minimo 1 caracter y máximo 3")
	@Column(name = "temperatura", nullable = false, length = 3)	
	private String temperatura;
	
	@ApiModelProperty(notes = "Pulso debe tener minimo 1 caracter y máximo 3")
	@Size(min = 1, max = 3, message = "Pulso debe tener minimo 1 caracter y máximo 3")
	@Column(name = "pulso", nullable = false, length = 3)	
	private String pulso;
	
	@ApiModelProperty(notes = "Ritmo respiratorio debe tener minimo 1 caracter y máximo 3")
	@Size(min = 1, max = 3, message = "Ritmo respiratorio debe tener minimo 1 caracter y máximo 3")
	@Column(name = "ritmo_respiratorio", nullable = false, length = 3)	
	private String ritmoRespiratorio;

	public int getIdSigno() {
		return idSigno;
	}

	public void setIdSigno(int idSigno) {
		this.idSigno = idSigno;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}

	public String getPulso() {
		return pulso;
	}

	public void setPulso(String pulso) {
		this.pulso = pulso;
	}

	public String getRitmoRespiratorio() {
		return ritmoRespiratorio;
	}

	public void setRitmoRespiratorio(String ritmoRespiratorio) {
		this.ritmoRespiratorio = ritmoRespiratorio;
	}

	
	/*
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idConsulta;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Signos other = (Signos) obj;
		if (idConsulta != other.idConsulta)
			return false;
		return true;
	}
	*/
	/*---------------------
	Consulta
	---------------------
	id | id_paciente | id_especialidad | id_medico | fecha
	--------------------------------------------------------
	1  |  1  		 | 1			   | 1			| hoy*/
}
